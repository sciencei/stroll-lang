{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PatternSynonyms #-}

module Language.Stroll.Parser where

import Language.Stroll.Types
import Text.Megaparsec hiding (State(..))
import Text.Megaparsec.Char
import Data.Text (Text)
import Data.Void (Void)
import qualified Data.Text as T
import qualified Text.Megaparsec.Char.Lexer as L
import Data.Dynamic (Typeable, fromDynamic)
import Control.Monad
import Data.Dynamic.Resolve (Ap(..), applyList, Tree(..), ApResult(..))
import Data.Dynamic.Resolve.Util (dynPureJoinId)
import qualified Data.HashMap.Lazy as H
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE
import Data.Dynamic (Dynamic(..))
import Control.Monad.State
import GHC.Records
import Data.Maybe (catMaybes, isJust)
import Type.Reflection (typeRep, TypeRep, pattern Fun, pattern App)
import Data.Foldable (foldl')

type Parser = ParsecT Void Text (State StrollState)
type TermMap = H.HashMap Text (NonEmpty Dynamic)
type AliasMap (atype :: ASTType) = H.HashMap Text (NonEmpty (Alias atype))

data StrollState = StrollState { functions :: TermMap
                               , aliases   :: TermMap
                               , fromObjs  :: [ObjParser]
                               }

data ObjParser where
    ObjParser :: forall a. TypeRep a -> (Text -> Maybe a) -> ObjParser

objParserToDynamic :: ObjParser -> Dynamic
objParserToDynamic (ObjParser tr v) = Dynamic (Fun (typeRep @Text) (App (typeRep @Maybe) tr)) v

parseObj :: ObjParser -> Text -> Maybe Dynamic
parseObj (ObjParser tr v) t = case (v t) of
                                Just v' -> Just $ Dynamic tr v'
                                Nothing -> Nothing

class (ParseObject atype, ParseFunction atype Untyped, ParseFunction atype (Typed Text)) 
  => ParseParam (atype :: ASTType) where
    param :: Parser (Param atype)
    param = choice [ SLit <$> stringLit
                   , choice [ILit <$> try integerLit, DLit <$> doubleLit]
                   , OLit <$> objectLit
                   , Func <$> function' False Nothing
                   ]

-- Basic building blocks for lexing

lineComment :: Parser ()
lineComment = L.skipLineComment "#"

scn :: Parser ()
scn = L.space space1 lineComment empty

sc :: Parser ()
sc = L.space (void $ takeWhile1P Nothing f) lineComment empty
  where f x = x == ' ' || x == '\t'

lx :: Parser a -> Parser a
lx = L.lexeme sc

symbol :: Text -> Parser Text
symbol = L.symbol sc

rws :: [Text]
rws = ["rule", "triggers", "actions"]

reserved :: Text -> Parser ()
reserved w = (lx . try) (string w *> notFollowedBy alphaNumChar)

reservedBlock :: Text -> Parser ()
reservedBlock w = reserved w *> symbol ":" *> pure ()

ident :: Parser Text
ident = (lx . try) (p >>= check)
  where
    p = T.pack <$> ((:) <$> letterChar <*> many alphaNumChar)
    check x = if x `elem` rws
                then fail $ T.unpack x ++ " is a reserved word and cannot be an identifier"
                else pure x

class ParseStringLit (atype :: ASTType) where
    escapedStringLit' :: Text -> Parser (StringLit atype)

    unescapedStringLit' :: Text -> Parser (StringLit atype)
    unescapedStringLit' end = (Literal . T.pack) <$> (manyTill anySingle $ string end)

    multilineString' :: Text -> Parser (StringLit atype) -> Parser (StringLit atype)
    multilineString' surrounds parser = do
        pos <- L.indentLevel
        try (string surrounds)
        newline
        (foldr (<>) mempty) <$> manyTill
          (   (spaceUntil pos) *> ((<> "\n") <$> parser)
          <|> (newline *> pure "")
          )
          (try (scn *> string surrounds))
      where
        matchPos pos = do
          current <- L.indentLevel
          if current >= pos
            then pure ()
            else fail "Indentation is less than that of the starting token"
        spaceUntil :: Pos -> Parser String
        spaceUntil pos = manyTill ((satisfy $ \c -> c == ' ' || c == '\t') <?> "more indentation") (matchPos pos)

    escapedStringLit :: Parser (StringLit atype)
    escapedStringLit = char '"' *> escapedStringLit' "\""

    unescapedStringLit :: Parser (StringLit atype)
    unescapedStringLit = char '\'' *> unescapedStringLit' "'"

    escapedMultilineString :: Parser (StringLit atype)
    escapedMultilineString = multilineString' "\"\"\"" $ escapedStringLit' "\n"

    unescapedMultilineString :: Parser (StringLit atype)
    unescapedMultilineString = multilineString' "'''" $ unescapedStringLit' "\n"

    stringLit :: Parser (StringLit atype)
    stringLit = lx ( choice [ unescapedMultilineString
                            , unescapedStringLit
                            , escapedMultilineString
                            , escapedStringLit
                            ]
                   ) <?> "string literal"

instance (ParseFunction atype (Typed Text)) => ParseStringLit atype where

  escapedStringLit' end = do
      text <- manyTill L.charLiteral $ lookAhead ((try $ string end) <|> (try $ string "{{"))
      choice [ string end *> pure (Literal $ T.pack text)
             , string "{{" 
                *> ( Interpolated (T.pack text)
                   <$> function' False Nothing
                   <*> ((string "}}") *> escapedStringLit' end)
                   )
             ]

class ParseFunction (atype :: ASTType) (functiontype :: FType) where
    function :: Parser (Function atype functiontype)
    function = function' False Nothing
    mkFunction :: Text -> [Param atype] -> Parser (Function atype functiontype)

    function' :: Bool -> Maybe (Param atype) -> Parser (Function atype functiontype)
    default function' :: (ParseParam atype, ParseFunction atype Untyped)
                      => Bool
                      -> Maybe (Param atype)
                      -> Parser (Function atype functiontype)
    function' allowIndent p = L.indentBlock scn parser
      where
        parser = do 
          name <- ident
          parenParams <- option [] $ parens $ sepBy1 param $ symbol ","
          let params = case p of
                         Just p' -> p':parenParams
                         Nothing -> parenParams
          this <- mkFunction name params
          let nest = (Just <$> Func <$> (mkFunction name params)) >>= function' allowIndent
          choice
            [ symbol "." *> (L.IndentNone <$> nest)
            , symbol ":" *> if allowIndent
                              then choice
                                [ (lookAhead newline) *>
                                    (pure $ L.IndentSome Nothing ((mkFunction name =<<) . pure . (params <>)) param)
                                , L.IndentNone <$> (mkFunction name =<< fmap ((params <>) . pure) param)
                                ]
                              else fail "Indented parameter blocks are not allowed inside parenthetical parameter lists."
            , pure $ L.IndentNone this
            ]

instance (ParseParam Abstract) => ParseFunction Abstract t where
    mkFunction = (pure .) . AbstractFunction

instance (ParseParam (Concrete m), Typeable m, Monad m, Display (Param (Concrete m)))
  => ParseFunction (Concrete m) Untyped where
    mkFunction name params = do
      tm <- get
      case H.lookup name $ functions tm of
        Nothing -> fail $ "No function found with name " ++ (T.unpack name)
        Just fs -> do
          let 
            dummyFunc = Func $ DynamicFunction name $ Ap fs $ Leaf $ OLit $ ConcreteObj "" fs
            results = applyList @m dummyFunc params
          case results of
            Success s -> pure $ DynamicFunction name s
            Failure (f, ps) -> fail $ T.unpack 
                                    $ mconcat [ "Could not apply '"
                                              , name <> "(" <> nameParams (applicationTree f) <> ")"
                                              , "' of possible types '"
                                              , T.intercalate "," $ NE.toList $ tshow <$> result f
                                              , "' to '"
                                              , nameParams $ applicationTree ps
                                              , "' of possible types '"
                                              , T.intercalate "," $ NE.toList $ tshow <$> result ps
                                              , "'."
                                              ]

instance (ParseParam (Concrete m), Typeable a, Typeable m, Monad m, Display (Param (Concrete m)))
    => ParseFunction (Concrete m) (Typed a) where
    mkFunction name params = do
      dynFun <- mkFunction @(Concrete m) @Untyped name params
      let results = dynPureJoinId @m <$> (result $ getField @"values" dynFun)
          tree = applicationTree $ getField @"values" dynFun
          reified = fromDynamic @(m a) <$> results
          fullname = name <> "(" <> nameParams tree <> ")"
      case catMaybes $ NE.toList reified of
        [] -> fail $ T.unpack
                   $ mconcat [ "Could not resolve function '"
                             , fullname
                             , "' to type '"
                             , tshow $ typeRep @a
                             , "'; possible valid types include: "
                             , T.intercalate "," $ NE.toList $ tshow <$> results
                             , "."
                             ]
        (l:ls) -> pure $ ConcreteFunction fullname (l:|ls)

tshow :: (Show a) => a -> Text
tshow = T.pack . show

nameParams :: (Display (Param (Concrete m))) => Tree (Param (Concrete m)) -> Text
nameParams t = T.intercalate "," $ tail $ display <$> foldr (:) [] t

class ParseObject (atype :: ASTType) where
  objectLit :: Parser (ObjectLit atype)
  default objectLit :: ParseFunction atype (Typed Text)
                    => Parser (ObjectLit atype)
  objectLit = do
    char '`'
    lit <- manyTill L.charLiteral $ lookAhead (try $ char '`')
    fromLiteral (T.pack lit)
  
  fromLiteral :: Text -> Parser (ObjectLit atype)

instance (ParseParam Abstract) => ParseObject Abstract where
    fromLiteral s = pure $ AbstractObj s

instance (Typeable m, Monad m, ParseParam (Concrete m)) => ParseObject (Concrete m) where
    fromLiteral s = do
      objs <- fromObjs <$> get
      let
        possObjs = foldl' (\acc x -> maybe acc (:acc) (parseObj x s)) [] objs
      case possObjs of
        [] -> fail "Could not parse object literal into object"
        ls -> pure $ ConcreteObj s $ NE.fromList ls

integerLit :: Parser Integer
integerLit = L.signed sc (lx L.decimal) <?> "integer literal"

doubleLit :: Parser Double
doubleLit = L.signed sc (lx L.float) <?> "floating point literal"

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")
