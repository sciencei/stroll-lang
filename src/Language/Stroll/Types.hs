{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Language.Stroll.Types 
 ( ASTType
 , Alias(..)
 , Concrete
 , Abstract
 , FType(Typed')
 , Typed
 , Untyped
 , Function(..)
 , StringLit(..)
 , ObjectLit(..)
 , Rule(..)
 , SyntaxTree(..)
 , Param(..)
 , Display(..)
 ) where

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE
import Data.Dynamic (Dynamic(..), Typeable, toDyn)
import Data.Text (Text(..))
import qualified Data.Text as T
import GHC.Generics (Generic)
import Type.Reflection (typeRep)
import Data.String (IsString(..))
import qualified Data.Dynamic.Resolve as R
import Control.Applicative (liftA2)

type Values m = R.Ap (Param (Concrete m))

data ASTType where
    Concrete' :: a -> ASTType
    Abstract' :: ASTType

type Concrete (m :: * -> *) = Concrete' m
type Abstract = Abstract'

data FType where
    Typed' :: a -> FType
    Untyped' :: FType

type Typed (t :: *) = Typed' t
type Untyped = Untyped'
    
data family Function (astType :: ASTType) (functiontype :: FType)

data instance Function Abstract t
    = AbstractFunction { name :: Text
                       , params :: [Param Abstract]
                       } deriving (Generic, Typeable)

data instance Function (Concrete m) Untyped
    = DynamicFunction { name :: Text
                      , values :: Values m
                      }

data instance Function (Concrete m) (Typed t)
    = ConcreteFunction { name :: Text
                       , values :: NonEmpty (m t)
                       }

data StringLit (atype :: ASTType)
    = Literal Text
    | Interpolated Text (Function atype (Typed Text)) (StringLit atype)

interpolate :: (Monad m) => StringLit (Concrete m) -> NonEmpty (m Text)
interpolate (Literal t) = pure $ pure t
interpolate (Interpolated pre ConcreteFunction{..} post) = (pre <>) <$$> values <+> (interpolate post)
  where
    (<$$>) = ((<$>) . (<$>))
    (<+>) = liftA2 $ liftA2 (<>)

instance IsString (StringLit atype) where
    fromString = Literal . T.pack

instance Semigroup (StringLit atype) where
    (Literal l) <> (Literal r) = Literal (l <> r)
    (Literal l) <> (Interpolated t f r) = Interpolated (l <> t) f r
    (Interpolated t f r) <> o = Interpolated t f (r <> o)

instance Monoid (StringLit atype) where
  mempty = Literal ""

data family ObjectLit (astType :: ASTType)

data instance ObjectLit Abstract
    = AbstractObj Text
    deriving (Show, Read, Eq, Generic, Typeable)

instance Display (ObjectLit Abstract) where
    display (AbstractObj t) = "`" <> t <> "`"

data instance ObjectLit (Concrete m)
    = ConcreteObj { literalText :: Text
                  , values  :: (NonEmpty Dynamic)
                  }

instance Display (ObjectLit (Concrete m)) where
    display (ConcreteObj t v) = "`" <> t 
                                    <> "`(possible types include: "
                                    <> (T.intercalate ", " $ display <$> NE.toList v) <> ")"

data SyntaxTree (atype :: ASTType) = SyntaxTree { rules   :: [Rule atype]
                                                , aliases :: [Alias atype]
                                                }

data Rule (atype :: ASTType)
    = Rule { name :: Text
           , description :: Maybe Text
           , triggers :: [Function atype (Typed Bool)]
           , actions :: [Function atype (Typed ())]
           } deriving (Typeable, Generic)

class Alias' (atype :: ASTType) where
    data Alias atype
    substitute :: Alias atype -> Param atype -> Maybe (Alias atype)

data Param (atype :: ASTType) 
    = SLit (StringLit atype)
    | ILit Integer
    | DLit Double
    | OLit (ObjectLit atype)
    | Func (Function atype Untyped)
    | List [Param atype]
    deriving (Generic, Typeable)

instance (Typeable m, Monad m) => R.Parameter (Param (Concrete m)) where
    values (SLit s) = pure $ toDyn $ interpolate s
    values (ILit i) = pure $ toDyn i
    values (DLit d) = pure $ toDyn d
    values (OLit (ConcreteObj{..})) = values
    values (Func DynamicFunction{..}) = R.result values

-- | A 'friendly' textual representation of a data type
-- , defaulting to `T.pack . show`
class Display a where
    display :: a -> Text

instance {-# OVERLAPPABLE #-} (Show a) => Display a where
    display = T.pack . show

instance (Display (ObjectLit atype), Display (StringLit atype), Display (Function atype Untyped))
  => Display (Param atype) where
    display (SLit s) =  display s
    display (ILit i) =  display i
    display (DLit d) =  display d
    display (OLit olit) = display olit
    display (Func f) = display f
