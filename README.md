A type safe, mostly declarative language designed for processing streams of data with 
a list of 'rules' (stroll = st(ream)+roll(rule)). It was designed with bots in
mind, but is generalizable. Stroll is designed to expose an explicit list of
Haskell functions to semi-technical users, which can then be used within Stroll.
Note that these functions must be monomorphic, as Stroll uses Dynamics internally.
Stroll 'compiles' to in-memory Haskell functions, which can then be run from within
a Haskell program using Stroll as a library.

Currently a WIP and unsuitable for actual use.

---
# DSL implementation details
The exposed DSL is defined with a `StrollState` object which includes a map from
function names to `NonEmpty Dynamic`s, and a list of `fromObj` functions, which must
be of the form `Text -> Maybe b`. All functions must either be pure, or live in
an 'environment' monad that must be the same between all monadic functions. Binding, lifting,
and joining is done automatically as needed to combine functions. The actual object
being streamed should be contained within the environment monad, to allow functions
to reference it implicitly.

---
# Syntax
Stroll has a roughly yaml-inspired syntax. Stroll files consist of a list of rules,
aliases, and imports.

## Imports

Example
```
import(other.stroll)
    blacklist:
        dangerousFn
    globalTriggers:
        isOther
    globalActions:
        logOther(otherName)
```
Stroll has an unusual import system to accomodate a sort of 'permissions system',
where some files can have more priveleges than others. Thus, by restricting who
can edit the files, you can restrict what exactly individual users are allowed to
do with the DSL.
Stroll is meant to begin compiling only one file, which then directly references
all the other files to be compiled with explicit imports. Each of these imports 
can be modified with a `blacklist` or `whitelist`, a list of `globalTriggers`,
and a list of `globalActions`

### Blacklist/Whitelist
Explicitly forbids or allows a list of aliases and functions that the imported
file can use. Note that it cannot whitelist functions that it itself does
not have access to.

### Global Triggers
Implicitly adds the listed triggers to all rules in the imported file

### Global actions
Implicitly adds the listed actions to all rules in the imported file

## Aliases

Example
```
alias(py)
    definition: (args) = shell("py", args)
```
Aliases are function definitions built up from existing functions. They are
primarily useful in conjunction with the permissions system mentioned in Imports;
using aliases, one can wrap an unsafe function to make a safer version, and then
only allow the safer version to be used by imported files. Aliases are compiled
at definition sites, and are type safe. After compilation, aliases are equivalent
to regular functions. Aliases must be defined earlier in the file than any uses of them.

## Rules
Example
```
rule(sample):
    after: earlierSample
    description: a sample rule
    triggers:
        isBlue
        body.contains: "blue thing"
        or: [ isComment
            , isPost
            ]
        eq:
            authorId
            `id_337`
    actions:
        reply("this is blue")
```

Rules are the meat of the language. Rules consist of an optional `description`,
an optional `after` or `insteadOf`, `triggers`, and `actions`

## Description
Simply a textual description of the rule; a function will be available within the
library to fetch a list of rule names and their descriptions, which could be used
for documentation purposes. Rules without desctiptions are considered 'hidden' and
will not be returned at all.

## after and insteadOf
Normally, rules are assumed to be independent of eachother. after and insteadOf
allow you to explicitly order one rule to execute after another one (using after),
or execute only if the other rule fails (using insteadOf).

## Triggers and Actions
Both triggers and actions consist of a list of function chains to be executed. In the case
of triggers, all function chains should resolve to a Bool. In the case of actions,
functions are assumed to be executed for their side effects and may resolve to any type.

### Functions
Functions consist of a function (or alias) name provided with parameters. Parameters
may be combined in a number of ways, which can be combined freely.

`func1.func2` passes the output of func1 as the first parameter of func2 and can
only pass one parameter.

`func2(param1, param2)` passes param1 as the first parameter and param2 as the second

`func2: param1` passes param1 as the first parameter and can only pass one parameter

```
func2:
    param1
    param2
```
passes param1 as the first parameter and param2 as the second parameter.

When using multiple methods of passing parameters, the `.` version has the highest
precedence, followed by the parameters in parentheses, followed by the parameters
after the semicolon. So for example in the following, each parameter is in the position
of the number it is labeled with

```
param1.func2(param2, param3):
            param4
            param5
```

### Parameters
Parameters may be any of the following:

#### Another function
Same as the rules for a regular function, but passing paremeters with the colon methods
is disallowed

#### String literal
Bookended by `"` for literals with escaping and `'` for literals without; everything
in a `'` literal is read literally. `"` literals can have interpolated sections
surrounded by `{{` and `}}`; these sections must contain functions which evaluate
to a string (implementation detail: will call `toString` function implicitly at 
the end of function application). Multiline strings can be made with `"""` or `'''`.

#### Object literal
Bookended by \` and evaluated at compile time into a typed object. Compilation
fails if the object cannot be evaluated.

#### Integer or Double literal
A signed integer or double

#### List literal
Bookended by `[` and `]`, with individual elements separated by commas. Can contain
any parameters within, but all contained parameters must be of the same type.


    